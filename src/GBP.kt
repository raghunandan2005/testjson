data class GBP(
    val currency_name: String,
    val rate: String,
    val rate_for_amount: String
)