
import com.google.gson.JsonDeserializer
import kotlin.Throws
import com.google.gson.JsonParseException
import com.google.gson.JsonElement
import com.google.gson.JsonDeserializationContext
import com.google.gson.Gson
import java.lang.reflect.Type


class NumberDeserializer : JsonDeserializer<NumberResponse> {
    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): NumberResponse {

        val gson = Gson()
        val jsonObject = json.asJsonObject
        val entry: MutableIterator<MutableMap.MutableEntry<String, JsonElement>> = jsonObject.entrySet().iterator()
        val list = mutableListOf<Number>()
        while(entry.hasNext()){
            val numberRes = jsonObject.getAsJsonObject(entry.next().key)
            val num: Number  = gson.fromJson(numberRes, Number::class.java)
            list.add(num)
        }
        return  NumberResponse(list)
    }
}