import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import com.google.gson.Gson

class CustomDeserializer: JsonDeserializer<ApiResponse> {
    override fun deserialize(json: JsonElement, typeT: Type, ctx: JsonDeserializationContext): ApiResponse {

        val gson = Gson()

        val apiResponse: ApiResponse = gson.fromJson(json, ApiResponse::class.java)
        val jsonObject = json.asJsonObject
        // parsing rates json object
        val ratesObject = jsonObject.getAsJsonObject("rates")
        val entry: Map.Entry<String, JsonElement> = ratesObject.entrySet().iterator().next()
        val codes = ratesObject.getAsJsonObject(entry.key)
        // parsing gbp json object
        val gbp: GBP = gson.fromJson(codes, GBP::class.java)

        val rates = Rates(gbp)
        apiResponse.rates = rates
        return apiResponse
    }
}