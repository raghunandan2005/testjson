import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

fun main() {

    val jsonString =
        "{\"amount\":\"10.0000\"," +
                "\"base_currency_code\":\"EUR\"," +
                "\"base_currency_name\":\"Euro\"," +
                "\"rates\"" +
                ":{\"GBP\"" +
                ":{\"currency_name\":\"Pound Sterling\",\"rate\":\"0.9060\",\"rate_for_amount\":\"9.0601\"}},\"status\":\"success\",\"updated_date\":\"2020-12-03\"}"


    val gson = GsonBuilder()
        .registerTypeAdapter(ApiResponse::class.java, CustomDeserializer())
        .create()
    val apiResponse = gson.fromJson(jsonString, ApiResponse::class.java)
    println(apiResponse)
    println(apiResponse.rates.gbp.currency_name)



    val json =
        "{\"1\":{\"id\":1,\"name\":\"ONE\"},\"2\":{\"id\":2,\"name\":\"TWO\"},\"3\":{\"id\":3,\"name\":\"THREE\"}}"

    val gson2 = GsonBuilder()
        .registerTypeAdapter(object : TypeToken<NumberResponse>() {}.type, NumberDeserializer())
        .create()
    val response = gson2.fromJson(json, NumberResponse::class.java)
    println(response)

    response.numberList.forEach {
        println("${it.id} and ${it.name}")
    }

}

